import Dependencies._
import build._

Global / onChangedBuildSource := ReloadOnSourceChanges

// Enable pinentry
Global / useGpgPinentry := true

lazy val `boring-logstage` = project
  .in(file("."))
  .configs(IntegrationTest)
  .settings(
    Defaults.itSettings,
    commonSettings,
    publishSettings,
    Seq(
      name := "boring-logstage",
      libraryDependencies ++= borer ++ `borer-test` ++ logstage ++ munit,
      Test / fork := true
    )
  )
