package is.solidninja.boring.logstage

import io.bullet.borer.Encoder
import is.solidninja.boring.logstage.LogstageBorerJsonCodec.genericWrite
import izumi.fundamentals.platform.exceptions.IzThrowable._
import izumi.logstage.api.rendering.{LogstageCodec, LogstageWriter}

class LogstageBorerJsonCodec[T](implicit val enc: Encoder[T]) extends LogstageCodec[T] {
  override def write(writer: LogstageWriter, value: T): Unit = {
    // this is quite ugly, but needed to avoid a roundtrip between the borer.Writer and the LogstageWriter
    writer match {
      case b: LogstageBorerJsonWriter =>
        val _ = enc.write(b.w, value)
      case _ =>
        // fallback - we are not going to be using the borer codec here but instead the built-in support
        genericWrite(writer, value)
    }
  }
}

object LogstageBorerJsonCodec {

  def apply[T: Encoder]() = new LogstageBorerJsonCodec[T]()

  /** Fallback codec for any scala structure (used only in the case where `LogstageBorerJsonCodec` is used without using
    * the `LogstageBorerJsonWriter`)
    */
  private[logstage] def genericWrite(w: LogstageWriter, v: Any): Unit = {
    v match {
      case null          => w.writeNull()
      case a: Double     => w.write(a)
      case a: Float      => w.write(a)
      case a: Long       => w.write(a)
      case a: Int        => w.write(a)
      case a: Short      => w.write(a)
      case a: Byte       => w.write(a)
      case a: Char       => w.write(a)
      case a: Boolean    => w.write(a)
      case a: String     => w.write(a)
      case a: BigDecimal => w.write(a)
      case t: Throwable  =>
        // note: also duplicated in encoderThrowable
        w.openMap()
        w.writeMapElement("type", Some(t.getClass.getName))
        w.writeMapElement("message", Option(t.getMessage))
        w.writeMapElement("stacktrace", Option[String](t.stackTrace))
        w.closeMap()
      case m: Map[_, _] =>
        w.openMap()
        m.foreach { case (k, v) =>
          w.nextMapElementOpen()
          genericWrite(w, k)
          w.mapElementSplitter()
          genericWrite(w, v)
          w.nextMapElementClose()
        }
        w.closeMap()
      case a: Array[_] =>
        w.openList()
        a.foreach { it =>
          w.nextListElementOpen()
          genericWrite(w, it)
          w.nextListElementClose()
        }
        w.closeList()
      case i: Iterable[_] =>
        w.openList()
        i.foreach { it =>
          w.nextListElementOpen()
          genericWrite(w, it)
          w.nextListElementClose()
        }
        w.closeList()
      case p: Product =>
        w.openMap()
        p.productElementNames.zipWithIndex.foreach { case (en, idx) =>
          w.nextMapElementOpen()
          w.write(en)
          w.mapElementSplitter()
          genericWrite(w, p.productElement(idx))
          w.nextMapElementClose()
        }
        w.closeMap()
      case _ =>
        // fallback, write to the toString repr
        w.write(v.toString)
    }
  }
}
