package is.solidninja.boring.logstage

import io.bullet.borer.{Encoder, Json, Writer}
import izumi.fundamentals.platform.exceptions.IzThrowable._
import izumi.fundamentals.platform.time.IzTime._
import izumi.logstage.api.Log
import izumi.logstage.api.Log.LogArg
import izumi.logstage.api.rendering.logunits.LogFormat
import izumi.logstage.api.rendering.{RenderedMessage, RenderedParameter, RenderingOptions, RenderingPolicy}

import scala.annotation.unused
import scala.runtime.RichInt

class LogstageBorerJsonRenderingPolicy(implicit enc: Encoder[Log.Entry]) extends RenderingPolicy {
  override def render(entry: Log.Entry): String = Json.encode(entry).toUtf8String
}

object LogstageBorerJsonRenderingPolicy {
  implicit val logEntryEncoder: Encoder[Log.Entry] = new Encoder[Log.Entry] {
    val EventKey = "event"
    val ContextKey = "context"
    val MetaKey = "meta"
    val TextKey = "text"
    val TimestampKey = "@timestamp"
    val MessageKey = "message"

    override def write(w: Writer, entry: Log.Entry): Writer = {
      val formatted = Format.formatMessage(entry, RenderingOptions(withExceptions = false, colored = false))

      val hasEventParams = formatted.parameters.nonEmpty || formatted.unbalanced.nonEmpty
      val hasContextParams = entry.context.customContext.values.nonEmpty

      // note: this apparently won't work for CBOR, however since the Renderer only does Strings it makes sense to use JSON first
      w.writeMapStart()

      // 1. meta information
      writeMeta(w, entry, formatted)

      // 2. text information
      writeText(w, entry, formatted)

      // 3. timestamp key (for Elasticsearch)
      w.writeString(TimestampKey)
      w.writeString(entry.context.dynamic.tsMillis.asEpochMillisUtc.isoFormatUtc.stripSuffix("[UTC]"))

      // 4. message key
      w.writeString(MessageKey)
      w.writeString(formatted.message)

      // 5. (optional) event parameters
      if (hasEventParams) {
        w.writeString(EventKey)
        writeParameters[RenderedParameter](w, formatted.parameters ++ formatted.unbalanced, _.normalizedName, repr)
      }

      // 6. (optional) context parameters
      if (hasContextParams) {
        w.writeString(ContextKey)
        writeParameters[LogArg](
          w,
          entry.context.customContext.values,
          _.name,
          (w, a) => repr(w, Format.formatArg(a, withColors = false))
        )
      }

      // finish up
      w.writeMapClose()
    }

    private def writeMeta(w: Writer, entry: Log.Entry, formatted: RenderedMessage): Unit = {
      w.writeString(MetaKey)
      w.writeMapStart()

      w.writeString("class")
      w.writeString(new RichInt(formatted.template.hashCode).toHexString)

      w.writeString("logger")
      w.writeString(entry.context.static.id.id)

      w.writeString("line")
      w.writeInt(entry.context.static.position.line)

      w.writeString("file")
      w.writeString(entry.context.static.position.file)

      w.writeString("level")
      w.writeString(entry.context.dynamic.level.toString.toLowerCase)

      w.writeString("timestamp")
      w.writeLong(entry.context.dynamic.tsMillis)

      w.writeString("datetime")
      w.writeString(entry.context.dynamic.tsMillis.asEpochMillisUtc.isoFormatUtc)

      // thread
      w.writeString("thread")
      w.writeMapStart()

      w.writeString("id")
      w.writeLong(entry.context.dynamic.threadData.threadId)

      w.writeString("name")
      w.writeString(entry.context.dynamic.threadData.threadName)

      w.writeMapClose()

      w.writeMapClose()
    }

    private def writeText(w: Writer, @unused entry: Log.Entry, formatted: RenderedMessage): Unit = {
      w.writeString(TextKey)
      w.writeMapStart()

      w.writeString("template")
      w.writeString(formatted.template)

      w.writeString("message")
      w.writeString(formatted.message)

      w.writeMapClose()
    }

    private def writeParameters[T](w: Writer, params: Seq[T], name: T => String, write: (Writer, T) => Unit): Unit = {
      val grouped = params.groupBy(name)
      val (unary, multiple) = grouped.partition(_._2.size == 1)

      w.writeMapStart()

      // unary
      unary.foreach { case (k, vs) =>
        w.writeString(k)
        write(w, vs.head)
      }

      // multiple (avoiding duplicates)
      multiple.foreach { case (k, vs) =>
        w.writeString(k)

        val vh = vs.head
        if (vs.tail.exists(_ != vh)) {
          w.writeArrayStart()
          vs.foreach(write(w, _))
          w.writeArrayClose()
        } else {
          write(w, vh)
        }
      }

      w.writeMapClose()
    }

    private def repr(w: Writer, param: RenderedParameter): Unit = {
      param.arg.codec match {
        case Some(codec) => LogstageBorerJsonWriter.write(w, codec, param.value)
        case None        => reprV(w, param.value, param)
      }
    }

    private def reprV(w: Writer, v: Any, param: RenderedParameter): Unit =
      v match {
        case null          => w.writeNull()
        case a: Double     => w.writeDouble(a)
        case a: Float      => w.writeFloat(a)
        case a: Long       => w.writeLong(a)
        case a: Int        => w.writeInt(a)
        case a: Short      => w.writeShort(a)
        case a: Byte       => w.writeByte(a)
        case a: Char       => w.writeChar(a)
        case a: Boolean    => w.writeBoolean(a)
        case a: String     => w.writeString(a)
        case a: BigDecimal => val _ = w.write(a)
        case a: Throwable  => val _ = w.write(a)
        case i: Iterable[_] =>
          w.writeArrayStart()
          i.foreach(it => reprV(w, it, null))
          w.writeArrayClose()
        case a: Array[_] =>
          w.writeArrayStart()
          a.foreach(it => reprV(w, it, null))
          w.writeArrayClose()
        case p: Product =>
          w.writeMapStart()
          p.productElementNames.zipWithIndex.foreach { case (en, idx) =>
            w.writeString(en)
            reprV(w, p.productElement(idx), null)
          }
          w.writeMapClose()
        case _ if param != null =>
          // fallback, write a string
          w.writeString(param.repr)
        case _ =>
          // fallback, write to the toString repr
          w.writeString(v.toString)
      }

  }

  implicit val encoderThrowable: Encoder[Throwable] = new Encoder[Throwable] {
    override def write(w: Writer, t: Throwable): Writer = {
      w.writeMapStart()
      w.writeString("type")
      w.writeString(t.getClass.getName)
      w.writeString("message")
      w.writeString(t.getMessage)
      w.writeString("stacktrace")
      w.writeString(t.stackTrace)
      w.writeMapClose()
    }
  }

  @inline def apply(): LogstageBorerJsonRenderingPolicy = new LogstageBorerJsonRenderingPolicy()

  object Format extends LogFormat.LogFormatImpl {
    override protected def toString(argValue: Any): String = {
      // TODO - how is this supposed to work???
      argValue match {
        case s: String => s
        case o         => o.toString
      }
    }
  }

}
