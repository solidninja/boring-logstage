package is.solidninja.boring.logstage

import io.bullet.borer.Writer
import izumi.logstage.api.rendering.{LogstageCodec, LogstageWriter}

class LogstageBorerJsonWriter(protected[logstage] val w: Writer) extends LogstageWriter {
  override def openList(): Unit = w.writeArrayStart()

  override def closeList(): Unit = w.writeArrayClose()

  override def openMap(): Unit = w.writeMapStart()

  override def closeMap(): Unit = w.writeMapClose()

  override def nextListElementOpen(): Unit = ()

  override def nextListElementClose(): Unit = ()

  override def nextMapElementOpen(): Unit = ()

  override def nextMapElementClose(): Unit = ()

  override def mapElementSplitter(): Unit = ()

  override def writeNull(): Unit = w.writeNull()

  override def write(a: Byte): Unit = w.writeByte(a)

  override def write(a: Short): Unit = w.writeShort(a)

  override def write(a: Char): Unit = w.writeChar(a)

  override def write(a: Int): Unit = w.writeInt(a)

  override def write(a: Long): Unit = w.writeLong(a)

  override def write(a: Float): Unit = w.writeFloat(a)

  override def write(a: Double): Unit = w.writeDouble(a)

  override def write(a: Boolean): Unit = w.writeBoolean(a)

  override def write(a: String): Unit = w.writeString(a)

  override def write(a: BigDecimal): Unit = {
    val _ = w.write(a)
  }

  override def write(a: BigInt): Unit = {
    val _ = w.write(a)
  }
}

object LogstageBorerJsonWriter {

  def write[T](writer: Writer, codec: LogstageCodec[T], value: T): Unit = {
    val jw = new LogstageBorerJsonWriter(writer)
    codec.write(jw, value)
  }
}
