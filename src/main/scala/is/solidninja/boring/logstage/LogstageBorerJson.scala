package is.solidninja.boring.logstage

import io.bullet.borer.Encoder
import izumi.logstage.api.rendering.LogstageCodec

import java.util.UUID

trait LogstageBorerJson extends LogstageBorerJsonLowPriority {
  type JsonRenderingPolicy = LogstageBorerJsonRenderingPolicy
  val RenderingPolicy: JsonRenderingPolicy = LogstageBorerJsonRenderingPolicy()

  def fromBorer[T: Encoder]: LogstageCodec[T] = LogstageBorerJsonCodec()
}

trait LogstageBorerJsonLowPriority {
  // types missing from the default logstage codecs
  implicit val logstageUuidCodec: LogstageCodec[UUID] =
    LogstageBorerJsonCodec()(Encoder.forString.contramap[UUID](_.toString))
}
