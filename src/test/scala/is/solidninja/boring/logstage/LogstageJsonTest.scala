package is.solidninja.boring.logstage

import io.bullet.borer.Dom.Element
import io.bullet.borer.{Dom, Json}

import java.util.UUID

class LogstageJsonTest extends munit.FunSuite {

  test("basic string message encoding should work") {
    val (app, sink) = TestApp.newWithJson
    app.doSomethingSimple()
    val logMessages = sink.elements
    assertEquals(logMessages.size, 1)

    val got = logMessages.head

    val expected =
      """
        {
          "meta": {
            "class": "61ae21c1",
            "logger": "is.solidninja.boring.logstage.TestApp.doSomethingSimple",
            "line": 41,
            "file": "TestApp.scala",
            "level": "info",
            "timestamp": 1610127430119,
            "datetime": "2021-01-08T17:37:10.119Z[UTC]",
            "thread": {
              "id": 1,
              "name": "main"
            }
          },
          "text": {
            "template": "Doing something simple",
            "message": "Doing something simple"
          },
          "@timestamp": "2021-01-08T17:37:10.119Z",
          "message": "Doing something simple",
          "context": {
            "borer": true
          }
        }"""

    assertEqualsIgnoringTimestamps(got, expected)
  }

  test("basic string message encoding with context should work") {
    val (app, sink) = TestApp.newWithJson
    app.doSomethingSimpleContext()
    val logMessages = sink.elements
    assertEquals(logMessages.size, 1)

    val got = logMessages.head

    val expected =
      """
        {
          "meta": {
            "class": "bff7ac54",
            "logger": "is.solidninja.boring.logstage.TestApp.doSomethingSimpleContext",
            "line": 45,
            "file": "TestApp.scala",
            "level": "info",
            "timestamp": 1610129891785,
            "datetime": "2021-01-08T18:18:11.785Z[UTC]",
            "thread": {
              "id": 1,
              "name": "main"
            }
          },
          "text": {
            "template": "Doing something simple with context",
            "message": "Doing something simple with context"
          },
          "@timestamp": "2021-01-08T18:18:11.785Z",
          "message": "Doing something simple with context",
          "context": {
            "envDetails": {
              "env": "TEST",
              "user": "idU",
              "pid": 22
            },
            "borer": true
          }
        }"""

    assertEqualsIgnoringTimestamps(got, expected)
  }

  test("complex message encoding with context should work") {
    val ctx = ComplexContext(
      fooList = List(
        Foo(
          a = 10,
          b = 23.215,
          c = 19.23f,
          bar = Map(
            "ahfksd" -> Bar("foobar", Array(2, 5, 5, 6), UUID.fromString("00000000-aabb-0000-0000-000000000000")),
            "fhjdsu" -> Bar("barfod", Array(12, 534, 1), UUID.fromString("00000000-ccdd-0000-0000-000000000000"))
          )
        ),
        Foo(
          a = 42,
          b = 129.12,
          c = 20f,
          bar = Map(
            "fasdf" -> Bar("ban", Array(2, 5, 5, 6), UUID.fromString("00000000-eeff-0000-0000-000000000000"))
          )
        )
      ),
      nameMap = Map(
        "foo" -> "bar",
        "string" -> "int",
        "name" -> "surname"
      )
    )

    val (app, sink) = TestApp.newWithJson
    app.doSomethingComplex(ctx)
    val logMessages = sink.elements
    assertEquals(logMessages.size, 1)

    val got = logMessages.head

    val expected =
      """
        {
          "meta": {
            "class": "6caabf7e",
            "logger": "is.solidninja.boring.logstage.TestApp.doSomethingComplex",
            "line": 50,
            "file": "TestApp.scala",
            "level": "info",
            "timestamp": 1610130016823,
            "datetime": "2021-01-08T18:20:16.823Z[UTC]",
            "thread": {
              "id": 1,
              "name": "main"
            }
          },
          "text": {
            "template": "Doing something complex with context ${meaning_of_life}",
            "message": "Doing something complex with context meaning_of_life=42"
          },
          "@timestamp": "2021-01-08T18:20:16.823Z",
          "message": "Doing something complex with context meaning_of_life=42",
          "event": {
            "meaning_of_life": 42
          },
          "context": {
            "envDetails": {
              "env": "TEST",
              "user": "idU",
              "pid": 22
            },
            "borer": true,
            "complex": {
              "fooList": [
                {
                  "a": 10,
                  "b": 23.215,
                  "c": 19.23,
                  "bar": {
                    "ahfksd": {
                      "s": "foobar",
                      "r": [
                        2,
                        5,
                        5,
                        6
                      ],
                      "u": "00000000-aabb-0000-0000-000000000000"
                    },
                    "fhjdsu": {
                      "s": "barfod",
                      "r": [
                        12,
                        534,
                        1
                      ],
                      "u": "00000000-ccdd-0000-0000-000000000000"
                    }
                  }
                },
                {
                  "a": 42,
                  "b": 129.12,
                  "c": 20.0,
                  "bar": {
                    "fasdf": {
                      "s": "ban",
                      "r": [
                        2,
                        5,
                        5,
                        6
                      ],
                      "u": "00000000-eeff-0000-0000-000000000000"
                    }
                  }
                }
              ],
              "nameMap": {
                "foo": "bar",
                "string": "int",
                "name": "surname"
              }
            }
          }
        }"""

    assertEqualsIgnoringTimestamps(got, expected)
  }

  private def assertEqualsIgnoringTimestamps(got: String, expected: String): Unit = {
    val actualMsg = Json.decode(got.getBytes("UTF-8")).to[Element].value
    val expectedMsg = Json.decode(expected.getBytes("UTF-8")).to[Element].value

    // ignore thread details because runner details are not stable (even when forking)
    compareMapElements(actualMsg, expectedMsg, Set("@timestamp", "timestamp", "datetime", "thread"))
  }

  private def compareMapElements(got: Dom.Element, exp: Dom.Element, ignoredKeys: Set[String]): Unit = {
    val actualMap = got match {
      case m: Dom.MapElem => m
      case _              => fail("got dom element not a map")
    }

    val expectedMap = exp match {
      case m: Dom.MapElem => m
      case _              => fail("expected dom element not a map")
    }

    val (actualSize, aki, avi) = actualMap.get
    val (expectedSize, eki, evi) = expectedMap.get

    assertEquals(actualSize, expectedSize, "sizes don't match")

    val avl = aki.zip(avi).toList
    val evl = eki.zip(evi).toList

    avl.zip(evl).foreach { case ((gotK, gotV), (expK, expV)) =>
      assertEquals(gotK, expK)

      val kName = gotK match {
        case s: Dom.StringElem => s.value
        case _                 => null
      }

      if (!ignoredKeys.contains(kName)) {
        gotV match {
          case _: Dom.MapElem => compareMapElements(gotV, expV, ignoredKeys)
          case _              => assertEquals(gotV, expV, s"values at key $gotK don't match")
        }
      }
    }
  }
}
