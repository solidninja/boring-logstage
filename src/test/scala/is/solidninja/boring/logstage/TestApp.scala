package is.solidninja.boring.logstage

import io.bullet.borer.Encoder
import izumi.logstage.api.rendering.LogstageCodec
import izumi.logstage.api.routing.ConfigurableLogRouter
import logstage.strict.IzStrictLogger
import is.solidninja.boring.logstage.json._
import io.bullet.borer.derivation.MapBasedCodecs._
import izumi.logstage.sink.ConsoleSink
import logstage.Log

import java.util.UUID

case class EnvDetails(env: String, user: String, pid: Int)

object EnvDetails {
  implicit val borerEncoder: Encoder[EnvDetails] = deriveEncoder
  implicit val logstageCodec: LogstageCodec[EnvDetails] = fromBorer
}

case class Bar(s: String, r: Array[Int], u: UUID)

case class Foo(a: Int, b: BigDecimal, c: Float, bar: Map[String, Bar])

case class ComplexContext(fooList: List[Foo], nameMap: Map[String, String])

object ComplexContext {
  implicit val uuidEncoder: Encoder[UUID] = Encoder.forString.contramap(_.toString)

  implicit val barEncoder: Encoder[Bar] = deriveEncoder
  implicit val fooEncoder: Encoder[Foo] = deriveEncoder
  implicit val complexEncoder: Encoder[ComplexContext] = deriveEncoder

  implicit val logstageCodec: LogstageCodec[ComplexContext] = fromBorer
}

class TestApp(log: IzStrictLogger) {
  val envDetails = EnvDetails("TEST", "idU", 22)

  def doSomethingSimple(): Unit = {
    log.info("Doing something simple")
  }

  def doSomethingSimpleContext(): Unit = {
    log("envDetails" -> envDetails).info("Doing something simple with context")
  }

  def doSomethingComplex(context: ComplexContext): Unit = {
    val meaningOfLife = 42
    log("envDetails" -> envDetails, "complex" -> context).info(s"Doing something complex with context $meaningOfLife")
  }

}

object TestApp {

  def newWithJson: (TestApp, InMemorySink) = {
    val renderingPolicy = LogstageBorerJsonRenderingPolicy()
    val sink = new InMemorySink(renderingPolicy)
    val router =
      ConfigurableLogRouter(threshold = Log.Level.Trace, sinks = List(ConsoleSink.text(colored = false), sink))
    val logger = IzStrictLogger(router)("borer" -> true)
    new TestApp(logger) -> sink
  }
}
