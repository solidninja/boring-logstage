package is.solidninja.boring.logstage

import izumi.logstage.api.Log
import izumi.logstage.api.logger.LogSink
import izumi.logstage.api.rendering.RenderingPolicy

import java.util.concurrent.ConcurrentLinkedQueue
import scala.jdk.CollectionConverters._

class InMemorySink(policy: RenderingPolicy) extends LogSink {
  private val buffer = new ConcurrentLinkedQueue[String]()

  def elements: List[String] = buffer.asScala.toList

  override def flush(e: Log.Entry): Unit = {
    val _ = buffer.add(policy.render(e))
  }
}
