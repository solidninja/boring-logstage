[![pipeline status](https://gitlab.com/solidninja/boring-logstage/badges/main/pipeline.svg)](https://gitlab.com/solidninja/boring-logstage/commits/main)

# `boring-logstage` - structured logging serializer for logstage using borer

This library provides a logging codec for [logstage] using [borer].
Only JSON is supported currently (primarily due to the interfaces in logstage
being string-based).

Nonetheless, the integration should provide some performance benefits compared
to [circe] due to the lack of intermediate AST when serializing.

The format differs from the `logstage-circe` in the following way:

* There is an additional top-level key `message` equivalent to `text.message`
* There is an additional top-level key `@timestamp` which Elasticsearch can parse
* Context parameters that are passed multiple times but are otherwise identical are rendered as a single value and not a list

These changes were made to make importing to Elasticsearch straightforward.

###  _Benchmarks TODO_

## Getting started

To use, add the following to `build.sbt`:

`libraryDependencies += "is.solidninja.boring" %% "boring-logstage" % "0.0.1-SNAPSHOT"`

The example below uses the `borer-derivation` library:

```scala
import io.bullet.borer.Encoder
import io.bullet.borer.derivation.MapBasedCodecs._
import is.solidninja.boring.logstage.LogstageBorerJsonRenderingPolicy
import is.solidninja.boring.logstage.json._
import logstage._
import logstage.strict.IzStrictLogger

case class Context(foo: String)

// derived (or otherwise) borer encoder
implicit val contextEncoder: Encoder[Context] = deriveEncoder

// derive the logstage encoder
implicit val logstageCodec: LogstageCodec[Context] = fromBorer

// setup log router (typically done at application startup)
val router = ConfigurableLogRouter(sink = ConsoleSink(LogstageBorerJsonRenderingPolicy))
val log: IzStrictLogger = IzStrictLogger(router)

// use logger
val ctx: Context = Context("foo")
log.info(s"context: $ctx")
```

## License

This project is licensed as [MIT][mit-license]. 


[borer]: https://sirthias.github.io/borer/
[circe]: https://github.com/circe/circe
[logstage]: https://izumi.7mind.io/logstage/index.html
[mit-license]: https://opensource.org/licenses/MIT
