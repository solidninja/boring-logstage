import sbt._

object Dependencies {

  object Versions {
    val borer = "1.6.2"
    val izumi = "1.0.1"
    val munit = "0.7.20"
  }

  val borer = Seq(
    "io.bullet" %% "borer-core" % Versions.borer
  )

  val `borer-test` = Seq(
    "io.bullet" %% "borer-derivation" % Versions.borer % "it,test"
  )

  val logstage = Seq(
    "io.7mind.izumi" %% "logstage-core" % Versions.izumi
  )

  val munit = Seq(
    "org.scalameta" %% "munit" % Versions.munit % "it,test"
  )
}
